#!/usr/bin/env python3
# Copyright 2019-2020 Collabora, Ltd
# SPDX-License-Identifier: BSL-1.0
# Author: Ryan Pavlik <ryan.pavlik@collabora.com>
"""Make a hwdb file. Requires an associated .rules file."""

from xrhardware.db import get_devices
from xrhardware.generate import generate_file_contents

if __name__ == "__main__":
    all_entries = '\n\n'.join(d.make_hwdb_entry() for d in get_devices())
    contents = '\n'.join((
        "# This hwdb file must be used with the corresponding rules file.",
        all_entries
    ))
    print(generate_file_contents(__file__, contents))
