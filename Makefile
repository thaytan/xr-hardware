#!/usr/bin/env make -f
# Copyright 2020 Collabora, Ltd
# SPDX-License-Identifier: BSL-1.0
# Author: Ryan Pavlik <ryan.pavlik@collabora.com>

RULES_FILE := 70-xrhardware.rules
HWDB_FILE := 70-xrhardware.hwdb
HWDB_RULES_FILE := 70-xrhardware-hwdb.rules

INSTALL ?= install

PYTHON ?= python3

all: $(RULES_FILE) $(HWDB_FILE) $(HWDB_RULES_FILE)
.PHONY: all

test:
	$(PYTHON) test-db.py
	$(PYTHON) -m flake8 .
.PHONY: test

$(RULES_FILE): make-udev-rules.py xrhardware/db.py xrhardware/generate.py
	$(PYTHON) $< > $@
$(HWDB_FILE): make-hwdb-file.py xrhardware/db.py xrhardware/generate.py
	$(PYTHON) $< > $@
$(HWDB_RULES_FILE): make-hwdb-rules-file.py xrhardware/generate.py
	$(PYTHON) $< > $@

clean:
	-rm -f $(RULES_FILE) $(HWDB_FILE) $(HWDB_RULES_FILE)
.PHONY: clean

# Currently does nothing, since the generated files get committed.
clean_package:
.PHONY: clean_package

RULES_DIR ?= /etc/udev/rules.d

install: $(RULES_FILE)
	$(INSTALL) -d $(DESTDIR)$(RULES_DIR)/
	$(INSTALL) -m 644 -D $(RULES_FILE) $(DESTDIR)$(RULES_DIR)/
.PHONY: install

install_package: $(RULES_FILE)
	$(INSTALL) -d $(DESTDIR)$(PREFIX)/lib/udev/rules.d
	$(INSTALL) -m 644 -D $(RULES_FILE) $(DESTDIR)$(PREFIX)/lib/udev/rules.d/
.PHONY: install_package

